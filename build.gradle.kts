import com.javarubberduck.awsplugin.AwsPlugin
import com.javarubberduck.awsplugin.AwsPluginExtension

plugins {
    kotlin("jvm") version "1.3.50"
}

group = "com.javarubberduck"
version = "1.0-SNAPSHOT"

buildscript {
    repositories {
        mavenLocal()
        dependencies {
            classpath("com.javarubberduck:aws-plugin:0.1-SNAPSHOT")
        }
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("com.amazonaws:aws-lambda-java-core:1.2.0")
    implementation("com.amazonaws:aws-lambda-java-events:2.1.0")
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.3.1")
    testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.3.1")
}

repositories {
    mavenCentral()
}

tasks.test {
    useJUnitPlatform()
}

java {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

apply<AwsPlugin>()

configure<AwsPluginExtension> {
    s3BucketName = project.name
    profileName = "personal"
    templatePath = "./resources/sam-template.yml"
    stackName = "kotlin-lambda"
    region = "eu-west-1"
}