pluginManagement {
    repositories {
        maven(url = "~/.mvn/repositories")
        gradlePluginPortal()
        ivy(url = "../ivy-repo")
    }
}