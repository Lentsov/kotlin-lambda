package handler

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class HelloWorldHandlerTest {

    @Test
    fun handleRequest() {
        val handler = HelloWorldHandler()
        val requestEvent = APIGatewayProxyRequestEvent()
        val context = null
        val invocationResult =  handler.handleRequest(requestEvent, context)
        assertEquals("Hello javarubberduck world", invocationResult.body)
        assertEquals(200, invocationResult.statusCode)
        assertEquals("text/plain", invocationResult.headers["Content-type"])
    }
}