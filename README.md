# kotlin-lambda

This repo contains simple aws lambda that replies with `Hello world`. It is written in kotlin.
It can be used as a reference or starting point for similar projects.

Used tools:
* kotlin
* aws java sdk
* gradle with kotlin dsl
* aws gradle plugin
* aws cli tool
